<?php

namespace Database\Factories;

use App\Models\Paquete;
use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Transportista;

class PaqueteFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Paquete::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {

        return [
            'direccion_entrega'=>$this->faker->address,
            'imagen'=>'paquete_por_defecto.jpg',
            'transportista_id' => Transportista::all()->random()->id,

            
        ];
    }
}
