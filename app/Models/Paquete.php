<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Transportista;

class Paquete extends Model
{
    use HasFactory;

    protected $table= 'paquetes';
    protected $guarded=[];

     public function transportistas(){
    	return $this->belongsTo(Transportista::class);
    }
}
