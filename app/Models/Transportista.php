<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Empresa;
use App\Models\Paquete;
use Carbon\Carbon;


class Transportista extends Model
{
    use HasFactory;
    protected $table= 'transportistas';
    protected $guarded=[];
    public function empresas(){
        return $this->belongsToMany(Empresa::class);
    }
    public function paquetes(){
        return $this->hasMany(Paquete::class);
    }

    public function getAños(){
    	$fechaFormateada=Carbon::parse($this->fechaPermisoConducir);
    	return $fechaFormateada->diffInYears(Carbon::now());
    }
}
