<nav class="navbar navbar-expand-lg navbar-dark fixed-top bg-info">
  <div class="container-fluid">
  <a class="navbar-brand" href="{{url('/')}}">Paquetería</a>
  <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav me-auto mb-2 mb-lg-0">
      {{--@if(Auth::check() )--}}
      
      <li class="nav-item">
        <a href="{{route('transportistas.index')}}" class="nav-link ">Todos los transportistas</a>
      </li>
      <li class="nav-item">
        <a href="{{route('paquetes.create')}}" class="nav-link {{ request()->routeIs('paquetes.create')? ' active' : ''}}">Nuevo paquete</a>
      </li>
    </ul>
    
  </div>
</div>
</nav>




