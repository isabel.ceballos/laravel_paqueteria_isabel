@extends('layouts.master')
@section('titulo')Show
@endsection
@section('contenido')
@if (session('mensaje'))
<div class="alert alert-success">
	{{session('mensaje')}}
</div>
@endif
<div class="d-flex justify-content-center">
	<div class="card" style="width: 50rem;">
		<img class="card-img-top img-fluid rounded-pill" src="{{asset('assets/imagenes/transportistas')}}/{{$transportista->imagen}}" alt="Card image cap" style="width: 30rem;">
		<div class="card-body">
			<h2 class="card-title">{{$transportista->nombre}} {{$transportista->apellidos}}</h2>
			<h5 class="card-title">Años permiso circulación:</h5>
			<p class="card-text"> {{$transportista->getAños()}}</p>
			<h5 class="card-title">Paquetes:</h5>
			@foreach($transportista->paquetes as $paquete)
				<p class="card-text">{{$paquete->id}}: {{$paquete->direccion_entrega}}</p>
			@endforeach
			
			
		</div>
	</div>
</div>
@endsection