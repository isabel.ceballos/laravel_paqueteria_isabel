@extends('layouts.master')
@section('titulo')Index
@endsection
@section('contenido')
<div class="d-flex justify-content-center">
	@foreach($transportistas as $clave=> $transportista)
	<div class="card-group">
		<div class="card">		
			<a href="{{ route('transportistas.show' , $transportista) }}">
				<img src="{{asset('assets/imagenes/transportistas')}}/{{$transportista->imagen}}" class="img-fluid rounded-pill" style="height:200px"/>
				<div class="card-body">
					<h4 style="min-height:45px;margin:5px 0 10px 0">{{$transportista->nombre}}
					</h4>					
				</div>
			</a>
			<p>Revisiones: {{$transportista->paquetes->count()}} pendientes de entrega.</p>
			
		</div>
	</div>
	@endforeach
</div>

@endsection