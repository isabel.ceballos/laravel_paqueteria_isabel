@extends('layouts.master')
@section('titulo')Crear
@endsection
@section('contenido')
<form action="{{ route('paquetes.store') }}" method="post" enctype="multipart/form-data">
	@csrf
	<div class="offset-md-3 col-md-6">
	<div class="card" style="width: 50rem;">		
			<div class="card-header text-center">
				<h2 class="card-title">Añadir nuevo paquete</h2>
			</div>
			<div class="form-group">		
				<label for="especie">Dirección de entrega del paquete</label>
				<input type="text" name="especie" id="especie" class="form-control" required>    
			</div>
			<div class="form-group">
				<label for="transportista_id">Transportista</label>
			<select name="transportista_id" id="transportista_id" class="form-control">
				@foreach($transportistas as $transportista)
				<option value="{{ $transportista->id }}">{{ $transportista->nombre }} {{ $transportista->apellidos}}</option>
				@endforeach
			</select>			
			<div class="form-group">
				<label for="imagen">Imagen</label>
				<input type="file" name="imagen" id="imagen" class="form-control" required>
			</div>
			<div class="form-group text-center">
				<button type="submit" class="btn btn-success" style="padding:8px 100px;margin-top:25px;">Añadir paquete</button>
			</div>
		</div>
	</div>
</form>
@endsection